package Lab7.Ex4;

import java.io.IOException;

public class testClass {
    public static void main(String[] args) {
        //test code
        CarFactory carFactory = new CarFactory();
        Car c1 = carFactory.createCar("BMW", 100000);
        Car c2 = carFactory.createCar("Audi", 200000);
        Car c3 = carFactory.createCar("Mercedes", 300000);

        c1.saveCar("BMW.txt");
        c2.saveCar("Audi.txt");
        c3.saveCar("Volkswagen.txt");
        System.out.println("\n");

        Car.readCar("BMW.txt");
        System.out.println("\n");

        // load
        try {
            Car c4 = Car.loadCar("BMW.txt");
            System.out.println(c4);
        }catch (IOException e)
        {
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    carFactory.printCars();


    }
}