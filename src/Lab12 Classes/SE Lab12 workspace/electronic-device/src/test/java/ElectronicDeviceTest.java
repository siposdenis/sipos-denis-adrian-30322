import org.junit.Test;

import static org.junit.Assert.*;

import edu.utcn.lab12.devices.*;

public class ElectronicDeviceTest {
	@Test
	public void testTurnON() {
		ElectronicDevice tv = new TV(5);
		
		tv.turnOn();
		assertEquals(true, tv.isPowered());
	}
}
