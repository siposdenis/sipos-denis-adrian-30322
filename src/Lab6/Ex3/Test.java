package Lab6.Ex3;


public class Test {
    public static void main(String[] args) {

        Bank banca = new Bank();
        banca.addAccount("Marius", 20);
        banca.addAccount("altMarius", 25);
        banca.addAccount("totMarius", 15);
        banca.addAccount("bMariusMaiSarac", 5);

        System.out.println("Print between a range");
        banca.printAccounts(10, 20);

        System.out.println("\nAll accounts based on balance: ");
        banca.printAccounts();

        System.out.println("\nMarius's account: \n");
        System.out.println(banca.getAccount("Marius"));
    }
}
