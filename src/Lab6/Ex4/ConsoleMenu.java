package Lab6.Ex4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Dictionary dict = new Dictionary();
        dict.addWord(new Word("test"), new Definition("stillTest"));
        int command = 1;

        while (command != 0) {
            System.out.println("\nWhat to do?");
            System.out.println("1 - Print dictionary \n 2 - Add To Dict \n 3 - Get Definitions \n 4 - Get A Definition \n 5 - Get All Words \n 0 for Exit");
            command = scanner.nextInt();
            switch (command) {
                case 1: {
                    for (Word word : dict.getAllWords()) {
                        System.out.println(word + " = " + dict.getDefinition(word));
                    }
                    break;
                }
                case 2: {
                    System.out.println("What word?");
                    String word = scanner.next();
                    Word w = new Word(word);
                    System.out.println("What is the definition for this word? ");
                    String deff = scanner.next();
                    Definition def = new Definition(deff);
                    dict.addWord(w, def);
                    System.out.println("Word added successfully!");

                }
                case 3:
                {
                    System.out.println(dict.getDefinitions());
                    break;
                }
                case 4:{
                    String wordd = scanner.next();
                    Word w1 = new Word(wordd);
                    try {
                        Definition d = dict.getDefinition(w1);
                        System.out.println("The definition for " + w1 + " is " + d);
                    }
                    catch (Exception exception){
                        System.out.println("No definition for this word!");
                    }
                    break;
                }
                case 5:{
                    System.out.println(dict.getAllWords());
                    break;
                }
            }
            }
        }
    }
