package Lab6.Ex4;

public class Definition {
    String description;

    public Definition(){this.description = "";}
    public Definition(String description){
        this.description = description;
    }

    @Override
    public String toString(){
        return description;
    }
}
