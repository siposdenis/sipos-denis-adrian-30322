package Lab6.Ex4;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Dictionary {
    HashMap<Word, Definition> hm = new HashMap<>();

    public void addWord(Word word, Definition definition)
    {
        this.hm.put(word,definition);
    }

    public Definition getDefinition(Word word){
        return hm.get(getList(word));
    }

    public Set<Word> getAllWords() {
        return hm.keySet();
    }

    public Collection<Definition> getDefinitions() {
        return hm.values();
    }

    private Word getList(Word word) {
        return hm.keySet().stream().filter(w -> w.equals(word)).findFirst().orElseThrow(null);
    }
}
