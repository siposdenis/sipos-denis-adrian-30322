package Lab6.Ex1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(){
        this.owner = "";
        this.balance = 0;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void Withdraw(double amount)
    {
        if(amount > this.balance)
        {
            this.balance = balance - amount;
            System.out.println("Funds withdrawn!");
        }
        else
        {
            System.out.println("Insufficient ammount!");
        }
    }

    public void Deposit(double amount)
    {
        this.balance = balance + amount;
        System.out.println("Funds deposited!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
