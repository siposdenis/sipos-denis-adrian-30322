package Lab6.Ex2;


public class Test {
    public static void main(String[] args) {

        Bank banca = new Bank();
        banca.addAccount("Marius", 20);
        banca.addAccount("altMarius", 25);
        banca.addAccount("totMarius", 15);
        banca.addAccount("bMariusMaiSarac", 5);

        banca.printAllAccounts();

        System.out.println("\n");
        System.out.println(banca.getAccount("Marius"));
        System.out.println("\n");

        System.out.println("Accounts btw. a range: ");
        banca.printAccounts(10,22);

        System.out.println("Sorted alphabetically: ");
        System.out.println(banca.getAllAccounts());

    }
}
