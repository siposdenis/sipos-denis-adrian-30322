package Lab2;

import java.util.Scanner;


public class Ex4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Length of vector: ");
        int N = scanner.nextInt();
        int temp = 0;
        int []a = new int[N];
        for(int i = 0; i < N; i++)
        {
            System.out.println("Enter element: ");
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < N-1; i++)
            for (int j = 0; j < N-i-1; j++)
                if (a[j] > a[j+1])
                {
                    // swap arr[j+1] and arr[j]
                    temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
        System.out.println("The largest element is: " + a[N-1]);

    }
}
