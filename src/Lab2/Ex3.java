package Lab2;

import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b; //margins
        int prime = 0;
        System.out.println("Please enter A: ");
        a = scanner.nextInt();
        System.out.println("Now enter B: ");
        b = scanner.nextInt();


        for(int i = a; i<=b; i++)
        {
            int flag = 1;
            for(int j = 2; j<=i/2; j++)
            {
                if(i % j == 0)
                {
                    flag = 0;
                    break;
                }
            }
            if(flag == 1)
            {
                System.out.println(i);
                prime++;
            }
        }

        System.out.println("Prime numbers: " + prime);
    }
}
