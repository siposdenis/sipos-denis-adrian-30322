package Lab2;

import java.util.Random;
import java.util.Scanner;

public class Ex7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        int randomNr = rand.nextInt(10);
        int chances = 3;

        System.out.println("Hello! This is a random number guess game! I hope you enjoy it!");
        System.out.println("Pick a number between 0 and 10! But take care, you only have 3 chances to win!");

        while(chances!=0)
        {
            int pickedNumber = scanner.nextInt();
            if(pickedNumber == randomNr)
            {
                System.out.println("You won!");
                break;
            }
            else
            {
                chances--;
                if(chances == 0)
                {
                    System.out.println("You lost! :(");
                    break;
                }
                if(pickedNumber > randomNr)
                {
                    System.out.println("Number too high, please try again! You have " + chances + " more chances!");
                }
                else
                {
                    System.out.println("Number too low, please try again!You have " + chances + " more chances! ");
                }
            }
        }
    }
}
