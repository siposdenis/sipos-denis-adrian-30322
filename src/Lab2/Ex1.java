package Lab2;

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        int a, b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first number: ");
        a = scanner.nextInt();
        System.out.println("Please enter the second number: ");
        b = scanner.nextInt();

        if(a > b)
            System.out.println("a is bigger than b");
        else
            System.out.println("b is bigger than a");

    }
}
