package Lab2;

import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {
        // iterative
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number: ");
        int n = scanner.nextInt();
        iterative(n);

        // recursive
        System.out.println("Let's use the recursive method!");
        System.out.println("Enter a number to generate the factorial: ");
        int n2 = scanner.nextInt();
        System.out.println("Recursive of n2 is: " + recursive(n2));
    }

    public static void iterative(int N){
        int fact = 1;
        for(int i = 2; i <= N; i++)
        {
            fact *= i;
        }
        System.out.println("Fact of n is: " + fact);
    }

    public static int recursive(int N)
    {
        if(N >= 1)
            return N*recursive(N-1);
        else
            return 1;
    }
}
