package Lab2;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        int a;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please choose a number: ");
        a = scanner.nextInt();

        if(a == 0)
            System.out.println("Zero!");
        else if(a == 1)
            System.out.println("One");
        else if(a == 2)
            System.out.println("Two");
        else if(a == 3)
            System.out.println("Three");
        else if(a == 4)
            System.out.println("Four");
        else if(a == 5)
            System.out.println("Five");
        else if(a == 6)
            System.out.println("Six");
        else if(a == 7)
            System.out.println("Seven");
        else if(a == 8)
            System.out.println("Eight");
        else if(a == 9)
            System.out.println("Nine");
        else
            System.out.println("Other");


        System.out.println("Now, let's use switch! Enter a number: ");
        int choose = scanner.nextInt();

        switch(choose)
        {
            case 0:
                System.out.println("Zero"); break;
            case 1:
                System.out.println("One"); break;
            case 2:
                System.out.println("Two");break;
            case 3:
                System.out.println("Three");break;
            case 4:
                System.out.println("Four");break;
            case 5:
                System.out.println("Five");break;
            case 6:
                System.out.println("Six");break;
            case 7:
                System.out.println("Seven");break;
            case 8:
                System.out.println("Eight");break;
            case 9:
                System.out.println("Nine");break;
        }
    }
}
