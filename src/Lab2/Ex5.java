package Lab2;

import java.util.Random;
import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        int []a = new int[10];
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i<a.length ;i++)
        {
            System.out.println("Enter an element: ");
            a[i] = rand.nextInt(20);
        }

        for(int i = 0 ; i < a.length-1; i++)
        {
            for(int j = 0 ; j < a.length-i-1; j++)
            {
                if(a[j] > a[j+1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
            }
        }

        for(int i = 0 ; i < a.length; i++)
        {
            System.out.println(a[i] + " ");
        }
    }
}
