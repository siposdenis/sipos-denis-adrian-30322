package Lab4.Ex3;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, Author author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }



    @Override
    public String toString() {
        return "Book name: " + this.name + " by " + author;
    }

    public static void main(String[] args) {
        Author a1 = new Author("Mihai Eminescu", "mihai.e@yahoo.com", 'm');
        Author a2 = new Author("Kristin Hannah", "kristin.h@gmail.com", 'f');

        Book b1 = new Book("Book1", a1, 50);
        Book b2 = new Book("Book2", a2, 75, 10);

        System.out.println("Book's price: " + b1.getPrice());
        System.out.println(b2.author.name);
        System.out.println(b1);
    }
}