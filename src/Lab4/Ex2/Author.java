package Lab4.Ex2;

public class Author {
    String name;
    String email;
    char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return name + "(" + gender + ")" + email;
    }

    public static void main(String[] args) {
        Author a1 = new Author("Mihai Eminescu", "mihai.e@yahoo.com", 'm');
        Author a2 = new Author("Kristin Hannah", "kristin.h@gmail.com", 'f');

        System.out.println(a1.toString());
        System.out.println(a2.email);
        System.out.println("Email changed: ");
        a2.setEmail("kriss@yahoo.com");
        System.out.println(a2.email);

    }
}
