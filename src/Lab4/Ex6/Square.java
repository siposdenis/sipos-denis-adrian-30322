package Lab4.Ex6;

public class Square extends Rectangle{

    public Square(){
        super(1,1);
    }
    public Square(double side){
        super(side, side);
    }

    public Square(double side, boolean filled, String color) {
        super(color, filled, side, side);
    }

    public double getSide()
    {
        double width = getWidth();
        return width; // or width, same thing
    }

    public void setSide(double side)
    {
        super.setLength (side); //because the attributes are no longer visible
        super.setWidth (side);
    }
    @Override
    public void setWidth(double side){
        super.setWidth (side);
    }
    @Override
    public void setLength(double side)
    {
        super.setLength (side);
    }

    @Override
    public String toString() {
        return "A Square with side: " + getSide() + " which is a subclass of " + super.toString();
    }
}
