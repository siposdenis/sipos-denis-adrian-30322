package Lab4.Ex6;

public class Shape {
    String color;
    boolean filled;

    public Shape()
    {
        color = "green";
        filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled()
    {
        if(filled)
        {
            return true;
        }
        return false;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "A shape with color of " + color + " and filled: " + filled;
    }
}
