package Lab4.Ex6;

public class TestClass {
    public static void main(String[] args) {
        Shape s1 = new Shape("red", true);
        Circle c1 = new Circle("green", false, 4);
        Rectangle r1 = new Rectangle(6,8);
        Square sq1 = new Square(4);

        System.out.println(s1);

        System.out.println("\nRadius of circle: " + c1.getRadius());
        System.out.println("\nArea and perimeter of circle: " + c1.getArea() + " " + c1.getPerimeter());
        System.out.println("\n Area and perimeter of rectangle: " + r1.getArea() + " " + r1.getPerimeter());
        System.out.println("\n To string of square: " + sq1);
    }
}
