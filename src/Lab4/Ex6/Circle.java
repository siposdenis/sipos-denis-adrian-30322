package Lab4.Ex6;

public class Circle extends Shape{
    double radius;

    public Circle()
    {
        super();
        radius = 1.0;
    }

    public Circle(double radius) {

        super();
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getPerimeter()
    {
        return 2*Math.PI*radius;
    }

    public double getArea()
    {
        return 2*(Math.PI)*Math.pow(2,radius);
    }

    @Override
    public String toString() {
        return "A Circle with radius: " + getRadius() + " which is a subclass of " + super.toString();
    }
}
