package Lab4.Ex4;


import java.util.Arrays;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return this.authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void printAuthors()
    {
        for(int i = 0; i < authors.length; i++)
        {
            System.out.println("Author " + i + " has name: " + authors[i].name);
        }
    }

    @Override
    public String toString() {
        return "Book name: " + this.name + " written by " + authors.length + " authors";
    }

    public static void main(String[] args) {
        Author[] authors = new Author[]{
            new Author("Mihai Eminescu", "mihai.e@yahoo.com", 'm'),
            new Author("Kristin Hannah", "kristin.h@gmail.com", 'f'),
            new Author("Leo Fritz", "leof@gmail.com", 'm'),
            new Author("Linda Cleo", "lindaC@gmail.com", 'f')
        };


        Book b1 = new Book("Book1", authors, 50);

        System.out.println(b1);
        System.out.println("Price: " + b1.getPrice());
        System.out.println("Quantity: " + b1.getQtyInStock());
        System.out.println("\n Authors that wrote the book: ");
        System.out.println("Book's name: " + b1.getName());

        b1.printAuthors();
        System.out.println(Arrays.toString(b1.getAuthors()));
    }
}