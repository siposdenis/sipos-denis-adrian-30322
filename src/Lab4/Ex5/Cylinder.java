package Lab4.Ex5;

public class Cylinder extends Circle {
    double height;

    public Cylinder()
    {
        super();
        height = 1.0;
    }

    public Cylinder(double height) {

        super();
        this.height = height;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return 2*Math.PI*radius*height+2*Math.PI*Math.pow(radius,2);
    }

    public double getVolume()
    {
        return Math.PI * Math.pow(radius, 2) * height;
    }
}
