    package Lab4.Ex5;

    public class TestClass {
        public static void main(String[] args) {
            Cylinder c1 = new Cylinder(5, 10);
            Cylinder c2 = new Cylinder(10, 4);
            Circle c3 = new Circle(4);

            System.out.println("Circle's area: " + c3.getArea());
            System.out.println("\nFirst cylinder's area: " + c1.getArea());
            System.out.println("\nSecond's cylinder's area: " + c2.getArea());
            System.out.println("\nFirst cyl.'s volume: " + c1.getVolume());

        }
    }
