package Lab4.Ex1;

public class Circle {
    double radius;
    String color;

    public Circle()
    {
        radius = 1.0;
        color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea()
    {
        return 2*(Math.PI)*Math.pow(radius,2);
    }

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(5.5);

        System.out.println(c2.getArea());
        System.out.println(c1.getRadius());
        System.out.println(c1.color);
    }
}
