package Lab5.Ex2;

public class ProxyImage implements Image {

    private Image realImage;
    private String fileName;
    private String imageType;

    public ProxyImage(String fileName, String imageType) {
        this.imageType = imageType;
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if (realImage == null) {
            if (imageType.equalsIgnoreCase("rotated")) {  // equalsIgnoreCase ignores case sensitivity
                realImage = new RotatedImage(fileName);
            }
            else if(imageType.equalsIgnoreCase("real")){
                realImage = new RealImage(fileName);
            }
        }
        if(realImage != null){
        realImage.display();
        }
    }
}
