package Lab5.Ex2;

public class Test {
    public static void main(String[] args) {
        RealImage r1 = new RealImage("file1");
        RotatedImage r2 = new RotatedImage("rotated1");
        ProxyImage p1 = new ProxyImage("proxy1", "rotated");
        ProxyImage p2 = new ProxyImage("proxy2", "real");

        p1.display();
        p2.display();


    }
}
