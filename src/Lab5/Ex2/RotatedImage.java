package Lab5.Ex2;

public class RotatedImage implements Image{
    private static String fileName;

    public RotatedImage(String fileName) {
        this.fileName = fileName;
    }

     public void display()
    {
        System.out.println("Display Rotated " + fileName);
    }


}
