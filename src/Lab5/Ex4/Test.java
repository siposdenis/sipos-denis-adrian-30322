package Lab5.Ex4;

import Lab5.Ex3.Controller;
import Lab5.Ex3.LightSensor;
import Lab5.Ex3.TemperatureSensor;

public class Test {
    public static void main(String[] args) {
        TemperatureSensor t1 = new TemperatureSensor();
        LightSensor s1 = new LightSensor();

        Lab5.Ex3.Controller c1 = new Controller(t1,s1);
        c1.control();
    }
}
