package Lab5.Ex1;

public class Square extends Rectangle{
    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public void setWidth(double side)
    {
        super.setWidth(side);
    }

    public void setLength(double side)
    {
        super.setLength(side);
    }

    public void setSide(double side)
    {
        super.setLength (side); //because the attributes are no longer visible
        super.setWidth (side);
    }

    public double getSide()
    {
        double side = getLength();
        return side;
    }

    @Override
    public String toString() {
        return "Square has color: " + this.color + " it is filled: " + this.filled + " and side of: " + this.getSide();
    }
}
