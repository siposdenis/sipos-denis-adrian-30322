package Lab5.Ex1;

public class Circle extends Shape{
    double radius;

    public Circle(){
        this.radius = 0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        return Math.PI * Math.pow(this.radius,2);
    }

    @Override
    public double getPerimeter(){
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString() {
        return "Circle color is: " + this.color + " its radius is " + this.radius + " and it is filled: " + this.filled;
    }
}
