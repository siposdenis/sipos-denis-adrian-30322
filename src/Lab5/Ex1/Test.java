package Lab5.Ex1;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        Circle c1 = new Circle("red", true, 5);
        Rectangle r1 = new Rectangle("blue", false, 3,4);
        Rectangle r2 = new Rectangle("green", true, 2,3);
        Square s1 = new Square("purple", false, 4);

        ArrayList<Shape> arr = new ArrayList<>();
        arr.add(c1);
        arr.add(r1);
        arr.add(r2);
        arr.add(s1);

        for(Shape s : arr)
        {
            System.out.println(s + "\n");
        }

    }
}
