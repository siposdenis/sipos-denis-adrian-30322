package Lab5.Ex3;

public class Controller {
    int PERIOD = 20;
    TemperatureSensor tempSensor;
    LightSensor lightSensor;

    public Controller(TemperatureSensor tempSensor, LightSensor lightSensor) {
        this.tempSensor = tempSensor;
        this.lightSensor = lightSensor;
    }

    public void control()
    {
        for(int i = 1 ; i <= PERIOD; i++)
        {
            System.out.println("Light sensor val at time: " + i + " is: " + lightSensor.readValue());
            System.out.println("Temperature sensor val at time: " + i + " is: " + tempSensor.readValue());
            System.out.println("\n");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
