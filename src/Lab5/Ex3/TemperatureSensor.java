package Lab5.Ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    @Override
    int readValue() {
        return (int)(Math.random()*100); // values btw 0-1 * 100 -> 0-100
    }
}
