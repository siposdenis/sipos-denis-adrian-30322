package Lab5.Ex3;

public class Test {
    public static void main(String[] args) {
        TemperatureSensor t1 = new TemperatureSensor();
        LightSensor s1 = new LightSensor();

        Controller c1 = new Controller(t1,s1);
        c1.control();
    }
}
