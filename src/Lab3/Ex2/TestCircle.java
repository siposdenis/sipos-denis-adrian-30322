package Lab3.Ex2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c = new Circle(3, "blue");
        Circle c2 = new Circle();

        System.out.println("First circle's area is: " + c.getArea());
        System.out.println("First circle's radius is: " + c.getRadius());
        System.out.println("Second circle's radius is: " + c2.getRadius());
    }
}
