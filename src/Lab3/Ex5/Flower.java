package Lab3.Ex5;

public class Flower{
    public static int counter;
    int petal;

    public Flower(int petal) {
        this.petal = petal;
        counter++;
    }

    public Flower(){
        System.out.println("Flower has been created!");
        counter++;
    }

    public static void numberOfConstr()
    {
        System.out.println(counter + " flowers have been created!");
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        Flower f1 = new Flower(4);
        numberOfConstr();
    }
}