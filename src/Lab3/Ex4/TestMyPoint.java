package Lab3.Ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint(3,4);
        MyPoint p2 = new MyPoint(1,2);
        MyPoint p3 = new MyPoint();

        System.out.println("First point coordinates: " + p1.toString());

        System.out.println("Third point coordinates: " + p3.toString() + "\n");
        System.out.println("Setting point nr3: ");
        p3.setXY(6,7);
        System.out.println("Third point coordinates: " + p3.toString());

        System.out.println("The distance between chosen points is: " + p2.distance(p1));
    }
}
