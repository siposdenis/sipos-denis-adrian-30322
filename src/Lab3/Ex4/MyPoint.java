package Lab3.Ex4;

public class MyPoint {
    public double x;
    public double y;

    public MyPoint()
    {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setXY(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double distance(MyPoint a1)
    {
        double dist = (a1.y - this.y) * (a1.y - this.y) + (a1.x - this.x) * (a1.x - this.x);
        double ssqrt = Math.sqrt(dist);
        return ssqrt;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
