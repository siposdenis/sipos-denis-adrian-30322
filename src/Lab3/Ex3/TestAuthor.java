package Lab3.Ex3;

public class TestAuthor {
    public static void main(String[] args) {
        Author a1 = new Author("Mihai Eminescu", "mihai_eminescu@yahoo.com", 'm');
        Author a2 = new Author("Elly Griffiths", "elly.griff@yahoo.com", 'f');

        System.out.println(a1.toString());
        System.out.println("Second author's email: " + a2.getEmail());
        System.out.println("\n");
        System.out.println("First author's gender: " + a2.getGender());

        a2.setEmail("anotherEmail@gmail.com");
        System.out.println("After changing email by using setter: ");
        System.out.println(a2.getEmail());

    }
}
