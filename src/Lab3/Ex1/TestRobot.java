package Lab3.Ex1;

public class TestRobot {
    public static void main(String[] args) {
        Robot r = new Robot();
        System.out.println(r); //toString is called automatically when printing the object
        r.change(4);
        System.out.println("After change: ");
        System.out.println(r);
    }
}
