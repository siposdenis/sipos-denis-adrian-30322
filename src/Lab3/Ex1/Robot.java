package Lab3.Ex1;

public class Robot {
    int x;

    public Robot() {
        x = 1;
    }

    public void change(int k)
    {
        if(k>=1)
            x+=k;
    }

    @Override
    public String toString() {
        return "Robot's position is: " + x;
    }
}
