package Lab9.Ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {

    JButton button;
    JTextField textF;

    Counter() {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);
        init();
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 125;
        int height = 20;

        textF = new JTextField("0");
        textF.setBounds(125, 125, width, height);

        button = new JButton("Increment!");
        button.setBounds(125, 175, width, height);
        button.addActionListener(
                e -> {
                    int value = Integer.parseInt(textF.getText());
                    value++;
                    textF.setText(String.valueOf(value));
                });

        add(button);
        add(textF);
    }

    public static void main(String[] args) {
        Counter c = new Counter();
    }

}

class buttonAction implements ActionListener {
    public void actionPerformed(ActionEvent e) {

    }
}