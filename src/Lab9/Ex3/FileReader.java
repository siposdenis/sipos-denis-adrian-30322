package Lab9.Ex3;

import javax.swing.*;

public class FileReader extends JFrame {

    JButton button;
    JTextArea tArea;

    FileReader() {
        setTitle("File Reader!");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 100;
        int height = 20;

        button = new JButton("Read!");
        button.setBounds(150, 150, width, height);
        button.addActionListener(e -> {
            tArea.setText("");
            try {
                String line;
                java.io.FileReader file = new java.io.FileReader("src\\Lab9\\Ex3\\test.txt");
                java.io.BufferedReader reader = new java.io.BufferedReader(file);
                while ((line = reader.readLine()) != null) {
                    tArea.append(line + "\n");
                }
                reader.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        tArea = new JTextArea();
        tArea.setBounds(100, 50, width + 100, height + 100);

        add(button);
        add(tArea);
    }

    public static void main(String[] args) {
        FileReader fileReader = new FileReader();
    }
}
