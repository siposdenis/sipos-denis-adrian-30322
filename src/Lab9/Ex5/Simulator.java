package Lab9.Ex5;
import javax.swing.*;
import java.util.*;

public class Simulator {

    /**
     * @param args
     */

    public static void main(String[] args) {

        Gui anouncmentPannel = Gui.getGui();
        ArrayList <Controler> controlers = new ArrayList<>();
        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        controlers.add(c1);
        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        controlers.add(c2);
        //build station Timisoara
        Controler c3 = new Controler("Timisoara");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        controlers.add(c3);
        //connect the controllers
        connectAllControlers(controlers);
        //testing

        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca","R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Timisoara","R-002");
        s8.arriveTrain(t3);

        displayStationStates(controlers);

        anouncmentPannel.jText.append("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            anouncmentPannel.jText.append("### Step "+i+" ###"+"\n");
            for(Controler c: controlers)
                c.controlStep();
            anouncmentPannel.jText.append("\n");

            displayStationStates(controlers);
        }
    }

    private static void displayStationStates(ArrayList<Controler> controlers)
    {
        for(Controler c: controlers)
            c.displayStationState();
    }
    private static void connectAllControlers(ArrayList<Controler> controlers)
    {
        for(Controler c: controlers)
            for(Controler cn: controlers)
                if(!c.equals(cn))
                    c.setNeighbourController(cn);
    }

}

class Controler{

    String stationName;
    Gui anouncmentPannel = Gui.getGui();

    ArrayList <Controler> neighbourControlers =new ArrayList<>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v){
        neighbourControlers.add(v);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();

                for(Controler controler : neighbourControlers)
                    if(t.getDestination().equals(controler.stationName)){
                        //check if there is a free segment
                        int id = controler.getFreeSegmentId();
                        if(id==-1){
                            anouncmentPannel.jText.append("Trenul +"+t.name+"din gara "+stationName+" nu poate fi trimis catre "+controler.stationName+". Nici un segment disponibil!\n");
                            return;
                        }
                        //send train
                        anouncmentPannel.jText.append("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+controler.stationName+"\n");
                        segment.departTRain();
                        controler.arriveTrain(t,id);
                    }
            }
        }//.for

    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    anouncmentPannel.jText.append("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName+"\n");
                    return;
                }else{
                    anouncmentPannel.jText.append("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName+"\n");
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        anouncmentPannel.jText.append("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!"+"\n");
    }


    public void displayStationState(){
        anouncmentPannel.jText.append("=== STATION "+stationName+" ===\n");
        for(Segment s:list){
            if(s.hasTrain())
                anouncmentPannel.jText.append("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|"+"\n");
            else
                anouncmentPannel.jText.append("|----------ID="+s.id+"__Train=______ catre ________----------|"+"\n");
        }
    }
}


class Segment{
    int id;
    Train train;

    Segment(int id){
        this.id = id;
    }

    boolean hasTrain(){
        return train!=null;
    }

    Train departTRain(){
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t){
        train = t;
    }

    Train getTrain(){
        return train;
    }
}

class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}


class Gui extends JFrame {


    private static Gui gui;

    //Control the accessible (allowed) instances
    public static Gui getGui() {
        if (gui == null) {
            gui = new Gui();
        }
        return gui;
    }
    JTextArea jText = new JTextArea();

    private Gui()
    {
        this.setTitle("Anoncement Board");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        init();
        this.setVisible(true);
    }
    void init()
    {
        this.setBounds(50,50,500,1050);
        jText.setEditable(false);
        jText.setBounds(30,30,430,970);
        add(jText);
    }



}