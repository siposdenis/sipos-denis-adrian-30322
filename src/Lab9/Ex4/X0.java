package Lab9.Ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class X0 extends JFrame {

    char player = 'x'; //first player's choice
    JButton[] buttons = new JButton[9];

    X0(){
        setTitle("X 0 Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,400);
        setVisible(true);
    }

    public void init(){
        setLayout(new GridLayout(3,3));
        for(int i = 0; i<=8;i++)        // initializing all buttons
        {
            buttons[i] = new JButton();
            buttons[i].setText("-");
            buttons[i].setBackground(Color.WHITE);
            buttons[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton buttonClicked = (JButton) e.getSource(); // get the clicked button
                    buttonClicked.setText(String.valueOf(player));  // change its name
                    buttonClicked.setEnabled(false);   //  block it so it cannot be changed

                    if(player == 'x'){
                        player = 'o';   // change to "o"
                        buttonClicked.setBackground(Color.BLUE);
                    }else{
                        player = 'x';   // change to x
                        buttonClicked.setBackground(Color.ORANGE);
                    }
                    displayWinner();    // check and display winner
                }
            });
            add(buttons[i]);
        }
    }

    private void resetTable()
    {
        player = 'x';
        for(int i = 0 ; i < 9; i++)
        {
            buttons[i].setText("-");
            buttons[i].setBackground(Color.white);
            buttons[i].setEnabled(true);
        }
    }

    private boolean draw(){
        boolean val = true;
        for(int i = 0 ; i < 9; i++)
        {
            if(buttons[i].getText().charAt(0) == '-'){
                val = false;
            }
        }
        return val;
    }

    public boolean checkRow(){
        int i = 0;
        for(int j = 0; j < 3; j++)
        {
            if(buttons[i].getText().equals(buttons[i+1].getText()) && buttons[i].getText().equals(buttons[i+2].getText()) && buttons[i].getText().charAt(0) != '-'){
                return true;
            }
            i=i+3;
        }
        return false;
    }

    public boolean checkColumn()
    {
        int i = 0;
        for(int j = 0; j < 3; j++)
        {
            if(buttons[i].getText().equals(buttons[i+3].getText()) && buttons[i].getText().equals(buttons[i+6].getText()) && buttons[i].getText().charAt(0) != '-'){
                return true;
            }
            i++;
        }
        return false;
    }

    public boolean checkDiag(){
        if(buttons[0].getText().equals(buttons[4].getText()) && buttons[0].getText().equals(buttons[8].getText())
                && buttons[0].getText().charAt(0) != '-')
            return true;
        else if(buttons[2].getText().equals(buttons[4].getText()) && buttons[2].getText().equals(buttons[6].getText())
                && buttons[2].getText().charAt(0) != '-')
            return true;
        else return false;
    }

    private boolean winner(){
        if(checkRow() == true || checkColumn() == true || checkDiag() == true) return true;
        else return false;
    }

    public void displayWinner() {
        if (winner() == true) {
            if (player == 'x')
                player = 'o';
            else
                player = 'x';

            JOptionPane jPane = new JOptionPane();  // nice mode to start over
            int result = JOptionPane.showConfirmDialog(jPane, "Winner: " + player + ", congrats! Wanna play again?", "Game over!", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_NO_OPTION)    // displays a "yes" "no" button pane
                resetTable();   // YES
            else
                System.exit(0); // NO
        }
        else if(draw()){
            JOptionPane jPane = new JOptionPane();
            int result = JOptionPane.showConfirmDialog(jPane, "Draw, play again!", "Game over!", JOptionPane.YES_NO_OPTION);
            if(result == JOptionPane.YES_NO_OPTION) resetTable();
            else System.exit(0);
        }
    }



    public static void main(String[] args) {
        X0 xo = new X0();
    }
}
